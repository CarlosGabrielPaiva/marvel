package com.marvel.core.data

import com.marvel.core.domain.Heroes
import retrofit2.http.GET
import retrofit2.http.Query

interface HeroesDataSource {

    @GET("v1/public/characters")
    suspend fun getHeroes(@Query("ts") ts: String,
                          @Query("apikey") apikey: String,
                          @Query("hash") hash: String,
                          @Query("limit") limit: String): Heroes
}