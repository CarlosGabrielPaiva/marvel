package com.marvel.core.data

import java.math.BigInteger
import java.security.MessageDigest
import java.sql.Timestamp

class HeroesRepository(private val dataSource: HeroesDataSource) {
    suspend fun getHeroes(quantityOfHeroes: String) = dataSource.getHeroes(
        getTimeStamp(),
        PUBLIC_KEY,
        getHashApiKey(),
        quantityOfHeroes
    )

    private fun getHashApiKey(): String {
        val timeStamp = Timestamp(System.currentTimeMillis()).time.toString()
        val input = timeStamp + PRIVATE_KEY + PUBLIC_KEY
        val messageDigest = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM)

        return BigInteger(MESSAGE_DIGEST_SIGNUM, messageDigest.digest(input.toByteArray()))
            .toString(MESSAGE_DIGEST_RADIX)
            .padStart(MESSAGE_DIGEST_PAD_START, MESSAGE_DIGEST_PAD_CHAR)
    }

    private fun getTimeStamp(): String {
        return Timestamp(System.currentTimeMillis()).time.toString()
    }

    companion object {
        private const val PUBLIC_KEY = "7b10a2541d178bd31ddafd00c089aeec"
        private const val PRIVATE_KEY = "1ad39cd5db69085785d857d4c49f4ad6c8de1928"
        private const val MESSAGE_DIGEST_ALGORITHM = "MD5"
        private const val MESSAGE_DIGEST_SIGNUM = 1
        private const val MESSAGE_DIGEST_RADIX = 16
        private const val MESSAGE_DIGEST_PAD_START = 32
        private const val MESSAGE_DIGEST_PAD_CHAR = '0'
    }
}