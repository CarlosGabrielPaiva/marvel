package com.marvel.core.interactors

import com.marvel.core.data.HeroesRepository

class GetHeroesUseCase(private val heroesRepository: HeroesRepository) {
    suspend operator fun invoke(quantityOfHeroes: String) =
        heroesRepository
            .getHeroes(quantityOfHeroes)
            .data.results
}