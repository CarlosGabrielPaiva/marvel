package com.marvel.core.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.reflect.TypeToken
import com.marvel.core.BaseUnitTest
import com.marvel.core.data.HeroesDataSource
import com.marvel.core.data.HeroesRepository
import com.marvel.core.domain.Heroes

import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.spyk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class HeroesRepositoryTest: BaseUnitTest() {

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    @get:Rule
    private var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var heroesRepository: HeroesRepository

    private lateinit var heroesDataSource: HeroesDataSource


    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)
        heroesDataSource = spyk()
        heroesRepository = spyk(HeroesRepository(heroesDataSource))
    }


    @Test
    fun `getHeroes - should return list with three heroes`() = runBlockingTest {
        coEvery { heroesDataSource.getHeroes(any(), any(), any(), any()) } returns getMockJson(
            HEROES_JSON, HEROES_TYPE
        )

        assertNotNull(heroesRepository.getHeroes("2"))
        assertEquals(3, heroesRepository.getHeroes("2").data.results.count())

    }

    companion object {
        private const val HEROES_JSON = "heroes_response.json"
        private val HEROES_TYPE =
            object : TypeToken<Heroes>() {}.type
    }
}