package com.marvel

class Constants {
    object Api {
        const val API_BASE_URL = "https://gateway.marvel.com/"
    }
}