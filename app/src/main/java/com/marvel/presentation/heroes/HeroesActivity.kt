package com.marvel.presentation.heroes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.marvel.R
import com.marvel.core.domain.HeroesResult
import com.marvel.databinding.ActivityHeroesBinding
import com.marvel.presentation.loading.LoadingDialogFragment
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.ArrayList

class HeroesActivity : AppCompatActivity() {

    private val viewModel: HeroesViewModel by viewModel()
    private val loading: LoadingDialogFragment by inject()
    private lateinit var binding: ActivityHeroesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHeroesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        configObservers()
        viewModel.getHeroes()

    }

    private fun configObservers() {
        viewModel.heroesState.observe(this, {
            when(it) {
                is HeroesState.Error -> configErrorDescription(it.exception.localizedMessage)
                is HeroesState.Loading -> configLoading(it.isLoading)
                is HeroesState.Success -> configRecyclerWithHeroes(it.heroesList)
            }
        })
    }

    private fun configErrorDescription(errorDescription: String?) {

    }

    private fun configLoading(isLoading: Boolean) {
        if (isLoading) {
            loading.show(supportFragmentManager, null)
        }
        loading.dismiss()
    }

    private fun configRecyclerWithHeroes(heroesList: ArrayList<HeroesResult>) {
        val heroesAdapter = HeroesAdapter(heroesList = heroesList)
        binding.recyclerViewHeroes.apply {
            layoutManager = LinearLayoutManager(this@HeroesActivity)
            adapter = heroesAdapter
        }
    }
}