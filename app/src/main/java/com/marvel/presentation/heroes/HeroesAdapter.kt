package com.marvel.presentation.heroes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marvel.R
import com.marvel.core.domain.HeroesResult

class HeroesAdapter(
    private val heroesList: List<HeroesResult>
    ): RecyclerView.Adapter<HeroesAdapter.HeroesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HeroesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.heroes_item, parent, false)
        return HeroesViewHolder(view)
    }

    override fun onBindViewHolder(holder: HeroesViewHolder, position: Int) {
        holder.bind(heroesList[position])
    }

    override fun getItemCount(): Int {
        return heroesList.count()
    }

    class HeroesViewHolder(view: View): RecyclerView.ViewHolder(view) {

        fun bind(hero: HeroesResult) {
            val thumbnailExtension = hero.thumbnail.extension
            val thumbnailPath = hero.thumbnail.path + "/portrait_xlarge.$thumbnailExtension"



        }
    }
}