package com.marvel.presentation.heroes

import com.marvel.core.domain.HeroesResult

sealed class HeroesState {
    data class Success(val heroesList: ArrayList<HeroesResult>): HeroesState()
    data class Error(val exception: Exception): HeroesState()
    data class Loading(val isLoading: Boolean): HeroesState()
}