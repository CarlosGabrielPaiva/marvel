package com.marvel.presentation.heroes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marvel.core.interactors.GetHeroesUseCase
import kotlinx.coroutines.launch

class HeroesViewModel(private var getHeroesUseCase: GetHeroesUseCase): ViewModel() {

    private val _heroesState = MutableLiveData<HeroesState>()

    val heroesState: LiveData<HeroesState>
      get() = _heroesState

    fun getHeroes() {
        _heroesState.postValue(HeroesState.Loading(isLoading = true))
        viewModelScope.launch {
            try {
               val heroes = getHeroesUseCase.invoke(quantityOfHeroes = "100")
                _heroesState.postValue(HeroesState.Success(heroesList = heroes))
            } catch (exception: Exception) {
                _heroesState.postValue(HeroesState.Error(exception = exception))
            }
            _heroesState.postValue(HeroesState.Loading(isLoading = false))
        }
    }
}