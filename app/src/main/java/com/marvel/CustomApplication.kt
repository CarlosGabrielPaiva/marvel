package com.marvel

import android.app.Application
import com.marvel.di.*
import com.marvel.di.viewModelModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class CustomApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@CustomApplication)
            modules(listOf(usesCase, viewModelModules,
                apiModule, connectionModule,
                viewsModules))
        }
    }
}