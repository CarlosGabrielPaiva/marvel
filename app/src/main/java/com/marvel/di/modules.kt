package com.marvel.di

import com.marvel.BuildConfig
import com.marvel.Constants
import com.marvel.core.data.HeroesDataSource
import com.marvel.core.data.HeroesRepository
import com.marvel.core.interactors.GetHeroesUseCase
import com.marvel.presentation.heroes.HeroesViewModel
import com.marvel.presentation.loading.LoadingDialogFragment
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val viewModelModules = module {
  viewModel { HeroesViewModel(get()) }
}

val viewsModules = module {
  single { LoadingDialogFragment() }
}

val usesCase = module {
  single { HeroesRepository(get()) }
  single { GetHeroesUseCase(get()) }
}

val apiModule = module {
  fun provideUserApi(retrofit: Retrofit): HeroesDataSource {
    return retrofit.create(HeroesDataSource::class.java)
  }

  single { provideUserApi(get()) }
}

val connectionModule = module {

  fun provideHttpClient(): OkHttpClient {
    val okHttpClientBuilder = OkHttpClient.Builder()

    return okHttpClientBuilder.build()
  }

  fun provideRetrofit(): Retrofit {
    return Retrofit.Builder()
      .baseUrl(BuildConfig.GATEWAY_BASE_URL)
      .addConverterFactory(GsonConverterFactory.create())
      .build()
  }

  single { provideHttpClient() }
  single { provideRetrofit() }

}