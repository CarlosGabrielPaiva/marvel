package com.marvel.viewModel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.gson.reflect.TypeToken
import com.marvel.BaseUnitTest
import com.marvel.core.data.HeroesDataSource
import com.marvel.core.data.HeroesRepository
import com.marvel.core.domain.Heroes
import com.marvel.core.interactors.GetHeroesUseCase
import com.marvel.presentation.heroes.HeroesState
import com.marvel.presentation.heroes.HeroesViewModel
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.catchException
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi

class HeroesViewModelTest: BaseUnitTest() {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var heroesViewModel: HeroesViewModel
    private lateinit var getHeroesUseCase: GetHeroesUseCase
    private lateinit var heroesRepository: HeroesRepository
    private lateinit var heroesDataSource: HeroesDataSource



    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        Dispatchers.setMain(testDispatcher)

        heroesDataSource = spyk()
        heroesRepository = spyk(HeroesRepository(dataSource = heroesDataSource))
        getHeroesUseCase = spyk(GetHeroesUseCase(heroesRepository = heroesRepository))
        heroesViewModel = spyk(HeroesViewModel(getHeroesUseCase))
    }


    @Test
    fun `getHeroes - should return list with three heroes`() = runBlockingTest {
        val heroesMock = getMockJson<Heroes>(HEROES_JSON, HEROES_TYPE).data.results
        coEvery { getHeroesUseCase.invoke(any()) } returns heroesMock

        heroesViewModel.getHeroes()

        Assert.assertEquals(3, getHeroesUseCase.invoke("1").count())
        coVerify { getHeroesUseCase.invoke(any()) }
        verify(exactly = 1) { heroesViewModel.getHeroes() }
        Assert.assertNotNull(heroesViewModel.heroesState)
        Assert.assertNotNull(heroesViewModel)

    }

    @Test(expected = Exception::class)
    fun `getHeroes - should return exception`() {
        val exception = catchException {  heroesViewModel.getHeroes() }

        heroesViewModel.getHeroes()

        Assert.assertEquals(heroesViewModel.heroesState, HeroesState.Error(exception))

    }

    companion object {
        private const val HEROES_JSON = "heroes_response.json"
        private val HEROES_TYPE =
            object : TypeToken<Heroes>() {}.type
    }
}